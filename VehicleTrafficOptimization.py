﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import random
from pylab import *
import matplotlib.pyplot as plt
from math import fmod, exp, log
from SimPy.Simulation import *
from random import expovariate, uniform, normalvariate
import pylab as pyl
import matplotlib.pyplot as plt
import numpy as np

VerdeNA=36.0
VerdeEAyOA=51.0
VerdeNB=13.0
VerdeEByOB=41.0
VerdeSB=29.0

# Inicialización de parámetros
J=0
LlegadasAcumNorteAIzq=0
LlegadasAcumNorteADer=0
LlegadasAcumOesteAIzq=0
LlegadasAcumOesteADer=0
LlegadasAcumEsteAIzq=0
LlegadasAcumEsteADer=0
LlegadasAcumNorteBIzq=0
LlegadasAcumNorteBDer=0
LlegadasAcumOesteBIzq=0
LlegadasAcumOesteBDer=0
LlegadasAcumEsteBIzq=0
LlegadasAcumEsteBDer=0
LlegadasAcumSurBIzq=0
LlegadasAcumSurBDer=0
NumeroIteracionesOptimizacion=100
TiempoTranscurridoSimulacion=0
TiempoArranque=2.0
TiempoDeEstudio=3600

TTEA=0.0
TTEB=0.0
TCA=0.0
TCB=0.0
ColaNorteA=0.0
ColaOesteA=0.0
ColaEsteA=0.0
ColaNorteB=0.0
ColaOesteB=0.0
ColaEsteB=0.0
ColaSurB=0.0
    
TiempoEntreSalidasAF1=0.0
TiempoEntreSalidasOesteAF2=0.0
TiempoEntreSalidasEsteAF2=0.0
TiempoEntreSalidasBF1=0.0
TiempoEntreSalidasBF2=0.0
TiempoEntreSalidasOesteBF3=0.0
TiempoEntreSalidasEsteBF3=0.0

SalidaNorteAIzq = 0.0
SalidaNorteADer = 0.0
SalidaOesteADer = 0.0
SalidaOesteAIzq = 0.0
SalidaEsteAIzq = 0.0
SalidaEsteADer = 0.0
SalidaNorteBIzq = 0.0
SalidaNorteBDer = 0.0
SalidaOesteBDer = 0.0
SalidaOesteBIzq = 0.0
SalidaEsteBIzq = 0.0
SalidaEsteBDer = 0.0
SalidaSurBIzq = 0.0
SalidaSurBDer = 0.0

SalidaDerechaNorteAIzq=0.0
SalidaDerechaNorteADer=0.0
SalidaDerechaOesteAIzq=0.0
SalidaDerechaOesteADer=0.0
SalidaDerechaEsteAIzq=0.0
SalidaDerechaEsteADer=0.0
SalidaDerechaNorteBIzq=0.0
SalidaDerechaNorteBDer=0.0
SalidaDerechaOesteBIzq=0.0
SalidaDerechaOesteBDer=0.0
SalidaDerechaEsteBIzq=0.0
SalidaDerechaEsteBDer=0.0
SalidaDerechaSurBIzq=0.0
SalidaDerechaSurBDer=0.0

SalidaRectoNorteAIzq=0.0
SalidaRectoNorteADer=0.0
SalidaRectoOesteAIzq=0.0
SalidaRectoOesteADer=0.0
SalidaRectoEsteAIzq=0.0
SalidaRectoEsteADer=0.0
SalidaRectoNorteBIzq=0.0
SalidaRectoNorteBDer=0.0
SalidaRectoOesteBIzq=0.0
SalidaRectoOesteBDer=0.0
SalidaRectoEsteBIzq=0.0
SalidaRectoEsteBDer=0.0
SalidaRectoSurBIzq=0.0
SalidaRectoSurBDer=0.0

SalidaIzquierdaNorteAIzq=0.0
SalidaIzquierdaNorteADer=0.0
SalidaIzquierdaOesteAIzq=0.0
SalidaIzquierdaOesteADer=0.0
SalidaIzquierdaEsteAIzq=0.0
SalidaIzquierdaEsteADer=0.0
SalidaIzquierdaNorteBIzq=0.0
SalidaIzquierdaNorteBDer=0.0
SalidaIzquierdaOesteBIzq=0.0
SalidaIzquierdaOesteBDer=0.0
SalidaIzquierdaEsteBIzq=0.0
SalidaIzquierdaEsteBDer=0.0
SalidaIzquierdaSurBIzq=0.0
SalidaIzquierdaSurBDer=0.0

PropSalidaIzquierdaNA=0.2909
PropSalidaRectoNA=0.3299
PropSalidaDerechaNA=0.3792
PropSalidaIzquierdaOA=0.0046
PropSalidaRectoOA=0.7532
PropSalidaDerechaOA=0.2422
PropSalidaIzquierdaEA=0.0037
PropSalidaRectoEA=0.9963
PropSalidaDerechaEA=0.0000
PropSalidaIzquierdaNB=0.0635
PropSalidaRectoNB=0.7590 
PropSalidaDerechaNB=0.1775 
PropSalidaIzquierdaOB=0.0049 
PropSalidaRectoOB=0.7543 
PropSalidaDerechaOB=0.2407 
PropSalidaIzquierdaEB=0.0025 
PropSalidaRectoEB=0.9231 
PropSalidaDerechaEB=0.0744 
PropSalidaIzquierdaSB=0.2807
PropSalidaRectoSB=0.7467
PropSalidaDerechaSB=0.0370
Amarillo=3.0
TodoRojo=1.0
TiempoAcumuladoCiclo = 0.0
NFasesA=2
NFasesB=3
minimoVerde=5.0

Ciclo=95 
maximoVerdeA=Ciclo-NFasesA*Amarillo-NFasesA*TodoRojo-(NFasesA-1)*minimoVerde
maximoVerdeB=Ciclo-NFasesB*Amarillo-NFasesB*TodoRojo-(NFasesB-1)*minimoVerde
CantVehiculosNorteAIzq=426 
CantVehiculosNorteADer=427
CantVehiculosOesteAIzq=478
CantVehiculosOesteADer=478
CantVehiculosEsteAIzq=470
CantVehiculosEsteADer=470

CantVehiculosNorteBIzq=167 
CantVehiculosNorteBDer=166
CantVehiculosOesteBIzq=450
CantVehiculosOesteBDer=450
CantVehiculosEsteBIzq=359
CantVehiculosEsteBDer=359
CantVehiculosSurBIzq=383
CantVehiculosSurBDer=383

# Suma de todos los vehículos que llegan a la intersección A desde cada una de sus tres calles
CantVehiculosA = CantVehiculosNorteAIzq+CantVehiculosNorteADer+CantVehiculosOesteAIzq+CantVehiculosOesteADer+CantVehiculosEsteAIzq+CantVehiculosEsteADer
# Suma de todos los vehículos que llegan a la intersección B desde cada una de sus Cuatro calles
CantVehiculosB = CantVehiculosNorteBIzq+CantVehiculosNorteBDer+CantVehiculosOesteBIzq+CantVehiculosOesteBDer+CantVehiculosEsteBIzq+CantVehiculosEsteBDer+CantVehiculosSurBIzq+CantVehiculosSurBDer

TiempoEntreSalidasMinAF1=1.2527*2 # Chequeado : )
TiempoEntreSalidasMinOesteAF2=1.1101*2
TiempoEntreSalidasMinEsteAF2=1.0972*2
TiempoEntreSalidasMinBF1=1.1156*2
TiempoEntreSalidasMinBF2=1.2060*2
TiempoEntreSalidasMinOesteBF3=1.1901*2
TiempoEntreSalidasMinEsteBF3=1.2689*2


TrancaAF1=0.1326584622
TrancaOesteAF2=	-0.043047236 # Salen mas de los que deben salir porque salen varios en rojo, por eso negativo
TrancaEsteAF2=	-0.02290157# Salen mas de los que deben salir porque salen varios en rojo, por eso negativo
TrancaBF1=	-0.096015222# Salen mas de los que deben salir porque salen varios en rojo, por eso negativo
TrancaBF2=	-0.185400882# Salen mas de los que deben salir porque salen varios en rojo, por eso negativo
TrancaOesteBF3=-0.4# Salen mas de los que deben salir porque salen varios en rojo, por eso negativo
TrancaEsteBF3=-0.06834294# Salen mas de los que deben salir porque salen varios en rojo, por eso negativo

# Proporción de flujo de entrada de cada uno de los carriles no sssssssssssssss
PropVehiculosNorteAIzq=CantVehiculosNorteAIzq*(1.0)/(CantVehiculosNorteAIzq*(1.0)+(1.0)*CantVehiculosNorteADer)
PropVehiculosNorteADer=CantVehiculosNorteADer*(1.0)/(CantVehiculosNorteAIzq*(1.0)+(1.0)*CantVehiculosNorteADer)
PropVehiculosOesteAIzq=CantVehiculosOesteAIzq*(1.0)/(CantVehiculosOesteAIzq*(1.0)+(1.0)*CantVehiculosOesteADer)
PropVehiculosOesteADer=CantVehiculosOesteADer*(1.0)/(CantVehiculosOesteAIzq*(1.0)+(1.0)*CantVehiculosOesteADer)
PropVehiculosEsteAIzq=CantVehiculosEsteAIzq*(1.0)/(CantVehiculosEsteAIzq*(1.0)+(1.0)*CantVehiculosEsteADer)
PropVehiculosEsteADer=CantVehiculosEsteADer*(1.0)/(CantVehiculosEsteAIzq*(1.0)+(1.0)*CantVehiculosEsteADer)
PropVehiculosNorteBIzq=CantVehiculosNorteBIzq*(1.0)/(CantVehiculosNorteBIzq*(1.0)+(1.0)*CantVehiculosNorteBDer)
PropVehiculosNorteBDer=CantVehiculosNorteBDer*(1.0)/(CantVehiculosNorteBIzq*(1.0)+(1.0)*CantVehiculosNorteBDer)
PropVehiculosOesteBIzq=CantVehiculosOesteBIzq*(1.0)/(CantVehiculosOesteBIzq*(1.0)+(1.0)*CantVehiculosOesteBDer)
PropVehiculosOesteBDer=CantVehiculosOesteBDer*(1.0)/(CantVehiculosOesteBIzq*(1.0)+(1.0)*CantVehiculosOesteBDer)
PropVehiculosEsteBIzq=CantVehiculosEsteBIzq*(1.0)/(CantVehiculosEsteBIzq*(1.0)+(1.0)*CantVehiculosEsteBDer)
PropVehiculosEsteBDer=CantVehiculosEsteBDer*(1.0)/(CantVehiculosEsteBIzq*(1.0)+(1.0)*CantVehiculosEsteBDer)
PropVehiculosSurBIzq=CantVehiculosSurBIzq*(1.0)/(CantVehiculosSurBIzq*(1.0)+(1.0)*CantVehiculosSurBDer)
PropVehiculosSurBDer=CantVehiculosSurBDer*(1.0)/(CantVehiculosSurBIzq*(1.0)+(1.0)*CantVehiculosSurBDer)
CantCiclos=38.0
TiempoDeSimulacion =4000
 
#Recurso utilizado para la simulación, es el que sirve a los vehículos
# Carriles, la capacidad 1, indica la cantidad maxima de vehículos atentidos simultaneamente 
NorteAIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
NorteADer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True) 
OesteADer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True) 
OesteAIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
EsteAIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
EsteADer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
NorteBIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True) 
NorteBDer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True) 
OesteBDer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True) 
OesteBIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
EsteBIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
EsteBDer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
SurBIzq = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True)
SurBDer = Resource(capacity = 1, name = "Carril", unitName = "Vehiculos",monitored=True) 

# Variables de estado (Esperas y colas), a las que se les va a calcular las estadísticas
# Tiempos en el carril
EsperaNorteA=Monitor(name="Espera")
EsperaOesteA=Monitor(name="Espera")
EsperaEsteA=Monitor(name="Espera")
EsperaNorteB=Monitor(name="Espera")
EsperaOesteB=Monitor(name="Espera")
EsperaEsteB=Monitor(name="Espera")
EsperaSurB=Monitor(name="Espera")

#Colas
colaNorteADer=Monitor(name="Cola")
colaNorteAIzq=Monitor(name="Cola")
colaOesteADer=Monitor(name="Cola")
colaOesteAIzq=Monitor(name="Cola")
colaEsteAIzq=Monitor(name="Cola")
colaEsteADer=Monitor(name="Cola")
colaNorteBDer=Monitor(name="Cola")
colaNorteBIzq=Monitor(name="Cola")
colaOesteBDer=Monitor(name="Cola")
colaOesteBIzq=Monitor(name="Cola")
colaEsteBIzq=Monitor(name="Cola")
colaEsteBDer=Monitor(name="Cola")
colaSurBIzq=Monitor(name="Cola")
colaSurBDer=Monitor(name="Cola")

#función generadora de vehículos, esta función tiene los tiempos entre llegadas y activa la clase proceso, en la cual se corre el tiempo de simulación allí ocurren las llegadas, esperas y salidas.
def generador():
    LlegadaNorteAIzq = 0
    LlegadaNorteADer = 0
    LlegadaOesteADer = 0
    LlegadaOesteAIzq = 0
    LlegadaEsteADer = 0
    LlegadaEsteAIzq = 0
    LlegadaNorteBIzq = 0
    LlegadaNorteBDer = 0
    LlegadaOesteBDer = 0
    LlegadaOesteBIzq = 0
    LlegadaEsteBDer = 0
    LlegadaEsteBIzq = 0
    LlegadaSurBDer = 0
    LlegadaSurBIzq = 0
    VectorLlegadasAcumNorteAIzq=[]
    VectorLlegadasAcumNorteADer=[]
    VectorLlegadasAcumOesteAIzq=[]
    VectorLlegadasAcumOesteADer=[]
    VectorLlegadasAcumEsteAIzq=[]
    VectorLlegadasAcumEsteADer=[]
    VectorLlegadasAcumNorteBIzq=[]
    VectorLlegadasAcumNorteBDer=[]
    VectorLlegadasAcumOesteBIzq=[]
    VectorLlegadasAcumOesteBDer=[]
    VectorLlegadasAcumEsteBIzq=[]
    VectorLlegadasAcumEsteBDer=[]
    VectorLlegadasAcumSurBIzq=[]
    VectorLlegadasAcumSurBDer=[]

    
    # INTERSECCIÓN A  
    for i in range(CantVehiculosNorteAIzq):
        LlegadaNorteAIzq=(TiempoDeEstudio+Ciclo)/CantVehiculosNorteAIzq+LlegadaNorteAIzq
        LlegadaVehiculoNorteAIzq = Proceso("%01d"%(i,)) 
        VectorLlegadasAcumNorteAIzq.append(LlegadaVehiculoNorteAIzq)
        activate(LlegadaVehiculoNorteAIzq,LlegadaVehiculoNorteAIzq.GeneracionLlegadasYSalidas(NorteAIzq),at = LlegadaNorteAIzq) 
     
    for i in range(CantVehiculosNorteADer):
        LlegadaNorteADer=(TiempoDeEstudio+Ciclo)/CantVehiculosNorteADer+LlegadaNorteADer
        LlegadaVehiculoNorteADer = Proceso("%01d"%(i,))
        VectorLlegadasAcumNorteADer.append(LlegadaVehiculoNorteADer.name)
        activate(LlegadaVehiculoNorteADer,LlegadaVehiculoNorteADer.GeneracionLlegadasYSalidas(NorteADer),at = LlegadaNorteADer)
    
    for i in range(CantVehiculosOesteAIzq):
        LlegadaOesteAIzq=(TiempoDeEstudio+Ciclo)/CantVehiculosOesteAIzq+LlegadaOesteAIzq
        LlegadaVehiculoOesteAIzq = Proceso("%01d"%(i,))
        VectorLlegadasAcumOesteAIzq.append(LlegadaVehiculoOesteAIzq.name)            
        activate(LlegadaVehiculoOesteAIzq,LlegadaVehiculoOesteAIzq.GeneracionLlegadasYSalidas(OesteAIzq),at = LlegadaOesteAIzq)

    for i in range(CantVehiculosOesteADer):
        LlegadaOesteADer=(TiempoDeEstudio+Ciclo)/CantVehiculosOesteADer+LlegadaOesteADer
        LlegadaVehiculoOesteADer = Proceso("%01d"%(i,))
        VectorLlegadasAcumOesteADer.append(LlegadaVehiculoOesteADer.name)
        activate(LlegadaVehiculoOesteADer,LlegadaVehiculoOesteADer.GeneracionLlegadasYSalidas(OesteADer),at = LlegadaOesteADer)
        

    # INTERSECCIÓN B

    for i in range(CantVehiculosNorteBIzq):
        LlegadaNorteBIzq=(TiempoDeEstudio+Ciclo)/CantVehiculosNorteBIzq+LlegadaNorteBIzq
        LlegadaVehiculoNorteBIzq = Proceso("%01d"%(i,))
        VectorLlegadasAcumNorteBIzq.append(LlegadaVehiculoNorteBIzq)
        activate(LlegadaVehiculoNorteBIzq,LlegadaVehiculoNorteBIzq.GeneracionLlegadasYSalidas(NorteBIzq),at = LlegadaNorteBIzq)

    for i in range(CantVehiculosNorteBDer):
        LlegadaNorteBDer=(TiempoDeEstudio+Ciclo)/CantVehiculosNorteBDer+LlegadaNorteBDer
        LlegadaVehiculoNorteBDer = Proceso("%01d"%(i,))
        VectorLlegadasAcumNorteBDer.append(LlegadaVehiculoNorteBDer.name)
        activate(LlegadaVehiculoNorteBDer,LlegadaVehiculoNorteBDer.GeneracionLlegadasYSalidas(NorteBDer),at = LlegadaNorteBDer)
               
    for i in range(CantVehiculosEsteBIzq):
        LlegadaEsteBIzq=(TiempoDeEstudio+Ciclo)/CantVehiculosEsteBIzq+LlegadaEsteBIzq
        LlegadaVehiculoEsteBIzq = Proceso("%01d"%(i,))
        VectorLlegadasAcumEsteBIzq.append(LlegadaVehiculoEsteBIzq.name)            
        activate(LlegadaVehiculoEsteBIzq,LlegadaVehiculoEsteBIzq.GeneracionLlegadasYSalidas(EsteBIzq),at = LlegadaEsteBIzq)

    for i in range(CantVehiculosEsteBDer):
        LlegadaEsteBDer=(TiempoDeEstudio+Ciclo)/CantVehiculosEsteBDer+LlegadaEsteBDer
        LlegadaVehiculoEsteBDer = Proceso("%01d"%(i,))
        VectorLlegadasAcumEsteBDer.append(LlegadaVehiculoEsteBDer.name)            
        activate(LlegadaVehiculoEsteBDer,LlegadaVehiculoEsteBDer.GeneracionLlegadasYSalidas(EsteBDer),at = LlegadaEsteBDer)

    for i in range(CantVehiculosSurBDer):
        LlegadaSurBIzq=(TiempoDeEstudio+Ciclo)/CantVehiculosSurBIzq+LlegadaSurBIzq
        LlegadaVehiculoSurBIzq = Proceso("%01d"%(i,))
        VectorLlegadasAcumSurBIzq.append(LlegadaVehiculoSurBIzq.name)            
        activate(LlegadaVehiculoSurBIzq,LlegadaVehiculoSurBIzq.GeneracionLlegadasYSalidas(SurBIzq),at = LlegadaSurBIzq)

    for i in range(CantVehiculosSurBDer):
        LlegadaSurBDer=(TiempoDeEstudio+Ciclo)/CantVehiculosSurBDer+LlegadaSurBDer
        LlegadaVehiculoSurBDer = Proceso("%01d"%(i,))
        VectorLlegadasAcumSurBDer.append(LlegadaVehiculoSurBDer.name)            
        activate(LlegadaVehiculoSurBDer,LlegadaVehiculoSurBDer.GeneracionLlegadasYSalidas(SurBDer),at = LlegadaSurBDer)

# Función para las estadísticas
def resultados():
    # Total cola y espera por intersección
    TTEA = EsperaNorteA.mean()+EsperaEsteA.mean()+EsperaOesteA.mean()
    TTEB =EsperaNorteB.mean()+EsperaEsteB.mean()+EsperaOesteB.mean()+EsperaSurB.mean()
    ColaNorteA=colaNorteAIzq.mean()+colaNorteADer.mean()
    ColaOesteA=colaOesteAIzq.mean()+colaOesteADer.mean()
    ColaEsteA=colaEsteAIzq.mean()+colaEsteADer.mean()
    ColaNorteB=colaNorteBIzq.mean()+colaNorteBDer.mean()
    ColaOesteB=colaOesteBIzq.mean()+colaOesteBDer.mean()
    ColaEste=colaEsteBIzq.mean()+colaEsteBDer.mean()
    ColaSurB=colaSurBIzq.mean()+colaSurBDer.mean()
    TCA= ColaNorteA+ColaOesteA+ColaEsteA
    TCB= ColaNorteB+ColaOesteB+ColaEsteB+ColaSurB
    # Guardado de las esperas y las colas
    return TTEA, TCA, TTEB, TCB

class Proceso(Process):

    def observe(self): 
        VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB=VectorLuces()  
        while True:
            global TiempoTranscurridoSimulacion, TiempoEntreSalidasAF1, TiempoEntreSalidasOesteAF2, TiempoEntreSalidasEsteAF2, TiempoEntreSalidasBF1, TiempoEntreSalidasBF2, TiempoEntreSalidasOesteBF3, TiempoEntreSalidasEsteBF3
            yield hold,self,1 
            TiempoDelCicloActual=TiempoTranscurridoSimulacion-TiempoAcumuladoCiclo
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):
                colaNorteAIzq.observe(y=len(NorteAIzq.waitQ)*2+len(NorteAIzq.activeQ)*2)
                colaNorteADer.observe(y=len(NorteADer.waitQ)*2+len(NorteADer.activeQ)*2)
                colaOesteAIzq.observe(y=len(OesteAIzq.waitQ)*2+len(OesteAIzq.activeQ)*2)
                colaOesteADer.observe(y=len(OesteADer.waitQ)*2+len(OesteADer.activeQ)*2)
                colaEsteAIzq.observe(y=len(EsteAIzq.waitQ)*2+len(EsteAIzq.activeQ)*2)
                colaEsteADer.observe(y=len(EsteADer.waitQ)*2+len(EsteADer.activeQ)*2)
                colaNorteBIzq.observe(y=len(NorteBIzq.waitQ)*2+len(NorteBIzq.activeQ)*2)
                colaNorteBDer.observe(y=len(NorteBDer.waitQ)*2+len(NorteBDer.activeQ)*2)
                colaOesteBIzq.observe(y=len(OesteBIzq.waitQ)*2+len(OesteBIzq.activeQ)*2)
                colaOesteBDer.observe(y=len(OesteBDer.waitQ)*2+len(OesteBDer.activeQ)*2)
                colaEsteBIzq.observe(y=len(EsteBIzq.waitQ)*2+len(EsteBIzq.activeQ)*2)
                colaEsteBDer.observe(y=len(EsteBDer.waitQ)*2+len(EsteBDer.activeQ)*2)
                colaSurBIzq.observe(y=len(SurBIzq.waitQ)*2+len(SurBIzq.activeQ)*2)
                colaSurBDer.observe(y=len(SurBDer.waitQ)*2+len(SurBDer.activeQ)*2)
            TiempoTranscurridoSimulacion=now()

            # Aqui se calcula el tiempo que el vehículo permanece en el canal de espera según el tiempo de la fase en el que llegó cada vehículo
            if (TiempoTranscurridoSimulacion>TiempoDeEstudio+Ciclo): # desde que inicia el ciclo hasta que se cumpla el verde
                TiempoEntreSalidasAF1=0
                TiempoEntreSalidasOesteAF2=0
                TiempoEntreSalidasEsteAF2=0
                TiempoEntreSalidasBF1=0
                TiempoEntreSalidasBF2=0
                TiempoEntreSalidasOesteBF3=0
                TiempoEntreSalidasEsteBF3=0
            else: 

                # INTERSECCIÓN A                                            FASE 1 
                if ((TiempoDelCicloActual)<=VerdeNA+Amarillo): # desde que inicia el ciclo hasta que se cumpla el verde
                    TiempoEntreSalidasAF1=TiempoEntreSalidasMinAF1+TrancaAF1
                else:
                    TiempoEntreSalidasAF1=abs(Ciclo-(TiempoDelCicloActual)+VerdeNA/2-Amarillo)

                # INTERSECCIÓN A                                            FASE 2
                if ((TiempoDelCicloActual)>VerdeNA+Amarillo+TodoRojo): # Tiempo entre los cuales esta la segunda fase
                    TiempoEntreSalidasOesteAF2=TiempoEntreSalidasMinOesteAF2+TrancaOesteAF2
                    TiempoEntreSalidasEsteAF2=TiempoEntreSalidasMinEsteAF2+TrancaEsteAF2
                else:
                    TiempoEntreSalidasOesteAF2=abs((VerdeNA+Amarillo+TodoRojo)-(TiempoDelCicloActual))
                    TiempoEntreSalidasEsteAF2=abs((VerdeNA+Amarillo+TodoRojo)-(TiempoDelCicloActual)+VerdeNA/2-TodoRojo)

            # INTERSECCIÓN B                                             FASE 1
                if ((TiempoDelCicloActual)<=VerdeNB+Amarillo):
                    TiempoEntreSalidasBF1=TiempoEntreSalidasMinBF1+TrancaBF1
                else:
                    TiempoEntreSalidasBF1=abs(Ciclo-(TiempoDelCicloActual))
                    
            # INTERSECCIÓN B                                             FASE 2
                if ((TiempoDelCicloActual)>VerdeNB+Amarillo+TodoRojo and (TiempoDelCicloActual)<=(VerdeNB+Amarillo+TodoRojo+VerdeSB+Amarillo)):
                    TiempoEntreSalidasBF2=TiempoEntreSalidasMinBF2+TrancaBF2 
                else:
                    TiempoEntreSalidasBF2=abs(TiempoDelCicloActual+VerdeSB-2*Amarillo-TodoRojo)

            # INTERSECCIÓN B                                             FASE 3
                if ((TiempoDelCicloActual)>(VerdeNB+Amarillo+TodoRojo+VerdeSB+Amarillo)):
                    TiempoEntreSalidasOesteBF3=TiempoEntreSalidasMinOesteBF3+TrancaOesteBF3
                    TiempoEntreSalidasEsteBF3=TiempoEntreSalidasMinEsteBF3+TrancaEsteBF3
                else:
                    TiempoEntreSalidasOesteBF3=abs(VerdeNB+Amarillo+TodoRojo+VerdeSB+Amarillo+TodoRojo-TiempoDelCicloActual)
                    TiempoEntreSalidasEsteBF3=abs(VerdeNB+Amarillo+TodoRojo+VerdeSB+Amarillo+TodoRojo-TiempoDelCicloActual)
        

    def GeneracionLlegadasYSalidas(self, Carril): 
        global TiempoTranscurridoSimulacion 
        global TiempoEntreSalidasAF1, TiempoEntreSalidasOesteAF2, TiempoEntreSalidasEsteAF2, TiempoEntreSalidasBF1, TiempoEntreSalidasBF2, TiempoEntreSalidasOesteBF3, TiempoEntreSalidasEsteBF3
        global EsperaNorteA, EsperaOesteA, EsperaEsteA, EsperaNorteB, EsperaOesteB, EsperaEsteB, EsperaSurB
        global SalidaNorteAIzq, SalidaNorteADer, SalidaOesteAIzq, SalidaOesteADer, SalidaEsteAIzq, SalidaEsteADer, SalidaNorteBIzq, SalidaNorteBDer, SalidaOesteBIzq, SalidaOesteBDer, SalidaEsteBIzq, SalidaEsteBDer, SalidaSurBIzq, SalidaSurBDer
        global ciclo 
        global SalidaIzquierdaNorteAIzq,    SalidaRectoNorteAIzq,   SalidaDerechaNorteAIzq
        global SalidaIzquierdaNorteADer,    SalidaRectoNorteADer,   SalidaDerechaNorteADer
        global SalidaIzquierdaOesteAIzq,    SalidaRectoOesteAIzq,   SalidaDerechaOesteAIzq
        global SalidaIzquierdaOesteADer,    SalidaRectoOesteADer,   SalidaDerechaOesteADer
        global SalidaIzquierdaEsteAIzq,     SalidaRectoEsteAIzq,    SalidaDerechaEsteAIzq
        global SalidaIzquierdaEsteADer,     SalidaRectoEsteADer,    SalidaDerechaEsteADer
        global SalidaIzquierdaNorteBIzq,    SalidaRectoNorteBIzq,   SalidaDerechaNorteBIzq
        global SalidaIzquierdaNorteBDer,    SalidaRectoNorteBDer,   SalidaDerechaNorteBDer        
        global SalidaIzquierdaOesteBIzq,    SalidaRectoOesteBIzq,   SalidaDerechaOesteBIzq
        global SalidaIzquierdaOesteBDer,    SalidaRectoOesteBDer,   SalidaDerechaOesteBDer
        global SalidaIzquierdaEsteBIzq,     SalidaRectoEsteBIzq,    SalidaDerechaEsteBIzq
        global SalidaIzquierdaEsteBDer,     SalidaRectoEsteBDer,    SalidaDerechaEsteBDer
        global SalidaIzquierdaSurBIzq,      SalidaRectoSurBIzq,     SalidaDerechaSurBIzq
        global SalidaIzquierdaSurBDer,      SalidaRectoSurBDer,     SalidaDerechaSurBDer
        global LlegadaVehiculoOesteBIzq
        global LlegadasAcumNorteAIzq,       LlegadasAcumNorteADer,        LlegadasAcumOesteAIzq, LlegadasAcumOesteADer, LlegadasAcumEsteAIzq, LlegadasAcumEsteADer,  LlegadasAcumNorteBIzq, LlegadasAcumNorteBDer, LlegadasAcumOesteBIzq, LlegadasAcumOesteBDer, LlegadasAcumEsteBIzq, LlegadasAcumEsteBDer, LlegadasAcumSurBIzq, LlegadasAcumSurBDer

        
        # INTESECCIÓN A
        
        # NORTE A
        # ********************************************************************************************************************** Izquierdo
        if Carril == NorteAIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasAF1=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumNorteAIzq<1 and (llegada-SalidaNorteAIzq)>TiempoEntreSalidasAF1: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasAF1=0
            LlegadasAcumNorteAIzq=LlegadasAcumNorteAIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasAF1*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaNorteAIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumNorteAIzq=LlegadasAcumNorteAIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosNorteAIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaNA): 
                SalidaIzquierdaNorteAIzq=SalidaIzquierdaNorteAIzq+1
                if (len(OesteBIzq.waitQ)<len(OesteBDer.activeQ)):
                    LlegadaVehiculoOesteBIzq = Proceso("%01d"%(SalidaIzquierdaNorteAIzq,))           
                    activate(LlegadaVehiculoOesteBIzq,LlegadaVehiculoOesteBIzq.GeneracionLlegadasYSalidas(OesteBIzq))
                else:
                    LlegadaVehiculoOesteBDer = Proceso("%01d"%(SalidaIzquierdaNorteAIzq,))           
                    activate(LlegadaVehiculoOesteBDer,LlegadaVehiculoOesteBDer.GeneracionLlegadasYSalidas(OesteBDer))                
            else:
                SalidaRectoNorteAIzq=SalidaRectoNorteAIzq+1
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaNorteA.observe(y=(SalidaNorteAIzq-llegada-TiempoEntreSalidasMinAF1))#(el tiempo de permanencia) - (lo qu¿e debe tardar en pasar cuando no hay cola)
            yield release, self, Carril# Libero el carril
        # ************************************************************************************************************************ Derecha
        elif Carril == NorteADer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasAF1=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumNorteADer<1 and (llegada-SalidaNorteAIzq)>TiempoEntreSalidasAF1: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasAF1=0
            LlegadasAcumNorteADer=LlegadasAcumNorteADer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasAF1*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaNorteAIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumNorteADer=LlegadasAcumNorteADer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosNorteADer)
            if(u<=PropSalidaIzquierdaNA): 
                SalidaIzquierdaNorteADer=SalidaIzquierdaNorteADer+1
            else:
                SalidaRectoNorteADer=SalidaRectoNorteADer+1
                #print "llegada", llegada-TiempoAcumuladoCiclo, "salida", SalidaNorteADer-TiempoAcumuladoCiclo, "Espera", SalidaNorteADer-llegada, "TiempoTranscurridoSimulacion", TiempoTranscurridoSimulacion,  "Cola MEDIA ", colaNorteADer.mean()#, "Cola", colaNorteADer
            yield release, self, Carril# Libero el carril

        # OESTE A
        # Izquierdo
        if Carril == OesteAIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasOesteAF2=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumOesteAIzq<1 and (llegada-SalidaOesteAIzq)>TiempoEntreSalidasOesteAF2: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasOesteAF2=0
            LlegadasAcumOesteAIzq=LlegadasAcumOesteAIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasOesteAF2*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaOesteAIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumOesteAIzq=LlegadasAcumOesteAIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosOesteAIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaOA):#(Lo que sale a la Izquierda del carril)/(Total del carril izquierdo)
                SalidaIzquierdaOesteAIzq=SalidaIzquierdaOesteAIzq+1
            else:
                SalidaRectoOesteAIzq=SalidaRectoOesteAIzq+1
                if (len(OesteBIzq.waitQ)<len(OesteBDer.activeQ)):
                    LlegadaVehiculoOesteBIzq = Proceso("%01d"%(SalidaIzquierdaOesteAIzq,))           
                    activate(LlegadaVehiculoOesteBIzq,LlegadaVehiculoOesteBIzq.GeneracionLlegadasYSalidas(OesteBIzq))
                else:
                    LlegadaVehiculoOesteBDer = Proceso("%01d"%(SalidaIzquierdaOesteAIzq,))           
                    activate(LlegadaVehiculoOesteBDer,LlegadaVehiculoOesteBDer.GeneracionLlegadasYSalidas(OesteBDer))    
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaOesteA.observe(y=(SalidaOesteAIzq-llegada-TiempoEntreSalidasMinOesteAF2))
            yield release, self, Carril# Libero el carril
        #  Derecha
        elif Carril == OesteADer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasOesteAF2=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumOesteADer<1 and (llegada-SalidaOesteAIzq)>TiempoEntreSalidasOesteAF2: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasOesteAF2=0
            LlegadasAcumOesteADer=LlegadasAcumOesteADer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasOesteAF2*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaOesteAIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumOesteADer=LlegadasAcumOesteADer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosOesteADer)
            if(u<=PropSalidaDerechaNA): 
                SalidaIzquierdaOesteADer=SalidaIzquierdaOesteADer+1
            else:
                SalidaRectoOesteADer=SalidaRectoOesteADer+1
                if (len(OesteBIzq.waitQ)<len(OesteBDer.activeQ)):
                    LlegadaVehiculoOesteBIzq = Proceso("%01d"%(SalidaRectoOesteADer,))           
                    activate(LlegadaVehiculoOesteBIzq,LlegadaVehiculoOesteBIzq.GeneracionLlegadasYSalidas(OesteBIzq))
                else:
                    LlegadaVehiculoOesteBDer = Proceso("%01d"%(SalidaRectoOesteADer,))           
                    activate(LlegadaVehiculoOesteBDer,LlegadaVehiculoOesteBDer.GeneracionLlegadasYSalidas(OesteBDer))                    
            yield release, self, Carril# Libero el carril            
        
        # Este A
        # ********************************************************************************************************************** Izquierdo
        if Carril == EsteAIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasEsteAF2=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumEsteAIzq<1 and (llegada-SalidaEsteAIzq)>TiempoEntreSalidasEsteAF2: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasEsteAF2=0
            LlegadasAcumEsteAIzq=LlegadasAcumEsteAIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasEsteAF2*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaVieja=SalidaEsteAIzq
            SalidaEsteAIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumEsteAIzq=LlegadasAcumEsteAIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosEsteAIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaEA):
                SalidaIzquierdaEsteAIzq=SalidaIzquierdaEsteAIzq+1
            else:
                SalidaRectoEsteAIzq=SalidaRectoEsteAIzq+1
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaEsteA.observe(y=(SalidaEsteAIzq-llegada-TiempoEntreSalidasMinEsteAF2))
            yield release, self, Carril# Libero el carril

        #  Derecha
        elif Carril == EsteADer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasEsteAF2=Ciclo-(TiempoTranscurridoSimulacion-llegada) # No los dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumEsteADer<1 and (llegada-SalidaEsteAIzq)>TiempoEntreSalidasEsteAF2: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasEsteAF2=0
            LlegadasAcumEsteADer	=LlegadasAcumEsteADer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasEsteAF2*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaEsteAIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumEsteADer=LlegadasAcumEsteADer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosEsteADer)
            if(u<=PropSalidaDerechaEA): # este tipo de proporcion no sssssssssssssss
                SalidaIzquierdaEsteADer=SalidaIzquierdaEsteADer+1
            else:
                SalidaRectoEsteADer=SalidaRectoEsteADer+1
            yield release, self, Carril# Libero el carril

        # NORTE B
        # Izquierdo
        if Carril == NorteBIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasBF1=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumNorteBIzq<1 and (llegada-SalidaNorteBIzq)>TiempoEntreSalidasBF1: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasBF1=0
            LlegadasAcumNorteBIzq=LlegadasAcumNorteBIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasBF1*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaNorteBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumNorteBIzq=LlegadasAcumNorteBIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosNorteBIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaNB):
                SalidaIzquierdaNorteBIzq=SalidaIzquierdaNorteBIzq+1
            else:
                SalidaRectoNorteBIzq=SalidaRectoNorteBIzq+1
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaNorteB.observe(y=(SalidaNorteBIzq-llegada-TiempoEntreSalidasMinBF1))
            yield release, self, Carril# Libero el carril
        # Derecha
        elif Carril == NorteBDer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasBF1=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumNorteBDer<1 and (llegada-SalidaNorteBIzq)>TiempoEntreSalidasBF1: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasBF1=0
            LlegadasAcumNorteBDer=LlegadasAcumNorteBDer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasBF1*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaNorteBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumNorteBDer=LlegadasAcumNorteBDer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosNorteBDer)
            if(u<=PropSalidaDerechaNB):
                SalidaIzquierdaNorteBDer=SalidaIzquierdaNorteBDer+1
                if (len(EsteAIzq.waitQ)<len(EsteADer.activeQ)):
                    LlegadaVehiculoEsteAIzq = Proceso("%01d"%(SalidaIzquierdaNorteBDer,))           
                    activate(LlegadaVehiculoEsteAIzq,LlegadaVehiculoEsteAIzq.GeneracionLlegadasYSalidas(EsteAIzq))
                else:
                    LlegadaVehiculoEsteADer = Proceso("%01d"%(SalidaIzquierdaNorteBDer,))           
                    activate(LlegadaVehiculoEsteADer,LlegadaVehiculoEsteADer.GeneracionLlegadasYSalidas(EsteADer))                                  
            else:
                SalidaRectoNorteBDer=SalidaRectoNorteBDer+1
            yield release, self, Carril# Libero el carril   
                 
        # Izquierdo
        if Carril == OesteBIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasOesteBF3=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumOesteBIzq<1 and (llegada-SalidaOesteBIzq)>TiempoEntreSalidasOesteBF3: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasOesteBF3=0
            LlegadasAcumOesteBIzq=LlegadasAcumOesteBIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasOesteBF3*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaOesteBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumOesteBIzq=LlegadasAcumOesteBIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosOesteBIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaOB):#(Lo que sale a la Izquierda del carril)/(Total del carril izquierdo)
                SalidaIzquierdaOesteBIzq=SalidaIzquierdaOesteBIzq+1
            else:
                SalidaRectoOesteBIzq=SalidaRectoOesteBIzq+1
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaOesteB.observe(y=(SalidaOesteBIzq-llegada-TiempoEntreSalidasMinOesteBF3))
            yield release, self, Carril# Libero el carril
        #  Derecha
        elif Carril == OesteBDer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasOesteBF3=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumOesteBDer<1 and (llegada-SalidaOesteBIzq)>TiempoEntreSalidasOesteBF3: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasOesteBF3=0
            LlegadasAcumOesteBDer=LlegadasAcumOesteBDer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasOesteBF3*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaOesteBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumOesteBDer=LlegadasAcumOesteBDer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosOesteBDer)
            if(u<=PropSalidaDerechaNA):
                SalidaIzquierdaOesteBDer=SalidaIzquierdaOesteBDer+1
            else:
                SalidaRectoOesteBDer=SalidaRectoOesteBDer+1
            yield release, self, Carril# Libero el carril

        # Izquierdo
        if Carril == EsteBIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasEsteBF3=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumEsteBIzq<1 and (llegada-SalidaEsteBIzq)>TiempoEntreSalidasEsteBF3: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasEsteBF3=0
            LlegadasAcumEsteBIzq=LlegadasAcumEsteBIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasEsteBF3*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaEsteBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumEsteBIzq=LlegadasAcumEsteBIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosEsteBIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaEB):
                SalidaIzquierdaEsteBIzq=SalidaIzquierdaEsteBIzq+1
            else:
                SalidaRectoEsteBIzq=SalidaRectoEsteBIzq+1
                if (len(EsteAIzq.waitQ)<len(EsteADer.activeQ)):
                    LlegadaVehiculoEsteAIzq = Proceso("%01d"%(SalidaIzquierdaNorteBDer,))           
                    activate(LlegadaVehiculoEsteAIzq,LlegadaVehiculoEsteAIzq.GeneracionLlegadasYSalidas(EsteAIzq))
                else:
                    LlegadaVehiculoEsteADer = Proceso("%01d"%(SalidaIzquierdaNorteBDer,))           
                    activate(LlegadaVehiculoEsteADer,LlegadaVehiculoEsteADer.GeneracionLlegadasYSalidas(EsteADer))   
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaEsteB.observe(y=(SalidaEsteBIzq-llegada-TiempoEntreSalidasMinEsteBF3))
            yield release, self, Carril# Libero el carril
        # Derecha
        elif Carril == EsteBDer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasEsteBF3=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumEsteBDer<1 and (llegada-SalidaEsteBIzq)>TiempoEntreSalidasEsteBF3: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasEsteBF3=0
            LlegadasAcumEsteBDer=LlegadasAcumEsteBDer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasEsteBF3*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaEsteBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumEsteBDer=LlegadasAcumEsteBDer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosEsteBDer)
            if(u<=PropSalidaDerechaEB):
                SalidaIzquierdaEsteBDer=SalidaIzquierdaEsteBDer+1
            else:
                SalidaRectoEsteBDer=SalidaRectoEsteBDer+1
                if (len(EsteAIzq.waitQ)<len(EsteADer.activeQ)):
                    LlegadaVehiculoEsteAIzq = Proceso("%01d"%(SalidaRectoEsteBDer,))           
                    activate(LlegadaVehiculoEsteAIzq,LlegadaVehiculoEsteAIzq.GeneracionLlegadasYSalidas(EsteAIzq))
                else:
                    LlegadaVehiculoEsteADer = Proceso("%01d"%(SalidaRectoEsteBDer,))           
                    activate(LlegadaVehiculoEsteADer,LlegadaVehiculoEsteADer.GeneracionLlegadasYSalidas(EsteADer))              
            yield release, self, Carril# Libero el carril   
            
        # Izquierdo
        if Carril == SurBIzq:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasBF2=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumSurBIzq<1 and (llegada-SalidaSurBIzq)>TiempoEntreSalidasBF2: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasBF2=0
            LlegadasAcumSurBIzq=LlegadasAcumSurBIzq+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasBF2*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaSurBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumSurBIzq=LlegadasAcumSurBIzq-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosSurBIzq)# Para distribución de los vehículos que salen
            if(u<=PropSalidaIzquierdaNA):
                SalidaIzquierdaSurBIzq=SalidaIzquierdaSurBIzq+1
                if (len(EsteAIzq.waitQ)<len(EsteADer.activeQ)):
                    LlegadaVehiculoEsteAIzq = Proceso("%01d"%(SalidaIzquierdaSurBIzq,))           
                    activate(LlegadaVehiculoEsteAIzq,LlegadaVehiculoEsteAIzq.GeneracionLlegadasYSalidas(EsteAIzq))
                else:
                    LlegadaVehiculoEsteADer = Proceso("%01d"%(SalidaIzquierdaSurBIzq,))           
                    activate(LlegadaVehiculoEsteADer,LlegadaVehiculoEsteADer.GeneracionLlegadasYSalidas(EsteADer))                 
            else:
                SalidaRectoSurBIzq=SalidaRectoSurBIzq+1
            if (TiempoTranscurridoSimulacion>Ciclo and TiempoTranscurridoSimulacion<TiempoDeEstudio+Ciclo):    
                EsperaSurB.observe(y=(SalidaSurBIzq-llegada-TiempoEntreSalidasMinBF2))
            yield release, self, Carril# Libero el carril
        # Derecha
        elif Carril == SurBDer:
            llegada = TiempoTranscurridoSimulacion*1.0 # Tiempo de la simulacion en el que llega el vehículo
            if TiempoTranscurridoSimulacion<Ciclo: #Acumulo carros para el 1° ciclo para que sea mi cola inicial
              TiempoEntreSalidasBF2=Ciclo-(TiempoTranscurridoSimulacion-llegada) # Nolos dejo salir hasta que se cumpla el 1° ciclo
            elif LlegadasAcumSurBDer<1 and (llegada-SalidaSurBIzq)>TiempoEntreSalidasBF2: #Si no hay cola y el último carro salió hace más tiempo que el tiempo entre salidas, el carro solo toma el tiempo de Arranque (si el carro está parado) y el tiempo desde que llega hasta que sale si el carro puede pasar y no tiene carros por delante.
                TiempoEntreSalidasBF2=0
            LlegadasAcumSurBDer=LlegadasAcumSurBDer+1 # Sumo la nueva llegada a la cola
            yield request, self, Carril  #Solicito el carril
            yield hold, self, TiempoEntreSalidasBF2*1.0 # Retraso el tiempo de simulación (min, ciclo, paso inmediato)
            SalidaSurBIzq=TiempoTranscurridoSimulacion+TiempoArranque #Tiempo en el q ocurre la salida, después de implementado el retraso
            LlegadasAcumSurBDer=LlegadasAcumSurBDer-1 # Descuento de la cola el vehículo que salió
            u=uniform(0,PropVehiculosSurBDer)
            if(u<=PropSalidaDerechaSB):
                SalidaIzquierdaSurBDer=SalidaIzquierdaSurBDer+1
            else:
                SalidaRectoSurBDer=SalidaRectoSurBDer+1
            yield release, self, Carril# Libero el carril     
                                        
class Ciclos(Process):
    def actualCiclo(self):
        global Ciclo, TiempoAcumuladoCiclo
        while True:
            yield hold, self, Ciclo#indicar el paso de un ciclo en el proceso
            TiempoAcumuladoCiclo = TiempoTranscurridoSimulacion+1

def VectorLuces():
    global VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB
    a=random.randrange(-5,5)
    VerdeNA=VerdeNA+a
    VerdeEAyOA=Ciclo-VerdeNA-2*Amarillo-2*TodoRojo #Aqui se ajusta para qu sume un ciclo
    b=random.randrange(-5,5)
    c=random.randrange(-5,5)
    VerdeNB=VerdeNB+b
    VerdeEByOB=VerdeEByOB+c             
    VerdeSB=Ciclo-VerdeEByOB-VerdeNB-3*Amarillo-3*TodoRojo
    print "a                      b          c", a, b, c
    print "VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB condicion", VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB  
    return VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB 
    
# Evaluación de toda la simulación
def Evaluacion():
    initialize()
    generador()
    sim=Proceso()
    activate(sim,sim.observe())
    ciclos = Ciclos()
    activate(ciclos,ciclos.actualCiclo())
    simulate(until = TiempoDeSimulacion)
    TTEA, TCA, TTEB, TCB=resultados()
    return TTEA, TCA, TTEB, TCB

# vectores necesarios para el recocido simulado
def VectoresParaRecocidoSimulado():
    global NumeroIteracionesOptimizacion
    TotalEsperaA=[]
    TotalEsperaB=[]
    TotalColaA=[]
    TotalColaB=[]
    J=[]
    TotalEspera=[]
    TotalCola=[]
    DeltaTotalEspera=[]
    DeltaTotalCola=[]
    DeltaJ=[] 
    for i in range(NumeroIteracionesOptimizacion):
        TTEA, TCA, TTEB, TCB=Evaluacion()
        TotalEsperaA.append(TTEA)
        TotalColaA.append(TCA)
        TotalEsperaB.append(TTEB)
        TotalColaB.append(TCB)
        TotalEspera.append((TotalEsperaA[i]+TotalEsperaB[i]))        
        TotalCola.append((TotalColaA[i]+TotalColaB[i]))           
        J.append(TotalEspera[i]+TotalCola[i])
        DeltaTotalEspera.append(TotalEspera[i]-TotalEspera[i-1])
        DeltaTotalCola.append(TotalCola[i]-TotalCola[i-1])
        DeltaJ.append(J[i]-J[i-1])
        pyl.savetxt(r".\TotalEsperaA.txt",[TotalEsperaA],"%02d"," ")
        pyl.savetxt(r".\TotalColaA.txt",[TotalColaA],"%02d"," ")     
        pyl.savetxt(r".\TotalEsperaB.txt",[TotalEsperaB],"%02d"," ")          
        pyl.savetxt(r".\TotalColaB.txt",[TotalColaB],"%02d"," ")         
        pyl.savetxt(r".\TotalEspera.txt",[TotalEspera],"%02d"," ")
        pyl.savetxt(r".\TotalCola.txt",[TotalCola],"%02d"," ")     
        pyl.savetxt(r".\J.txt",[J],"%02d"," ")          
    plt.plot(TotalEsperaA, 'g-*') 
    plt.plot(TotalEsperaB, 'c-*')   
    plt.plot(TotalEspera,  'b-*')   
    pyl.title('Total Esperas (seg) vs. Iteraciones (adim.)')
    xlabel('Iteraciones')
    ylabel('TTEA TTEB TTE (seg) ')
    plt.grid(True,which="both",ls="-")
    show()
    plt.plot(TotalColaA,  'm-^')   
    plt.plot(TotalColaB, 'y-^')
    plt.plot(TotalCola, 'k-^')   
    pyl.title('Total Colas (vehiculos) vs. Iteraciones (adim.)')
    xlabel('Iteraciones')
    ylabel('TCA TCB TC (seg) ')
    plt.grid(True,which="both",ls="-")
    show()    
    return TotalEsperaA, TotalEsperaB, TotalColaA, TotalColaB, J, TotalEspera, TotalCola, DeltaTotalEspera, DeltaTotalCola, DeltaJ 
TotalEsperaA, TotalEsperaB, TotalColaA, TotalColaB, J, TotalEspera, TotalCola, DeltaTotalEspera, DeltaTotalCola, DeltaJ=VectoresParaRecocidoSimulado()

# Recocido Simulado
alfa=0.95
DeltaPositivaEspera=0
DeltaPositivaCola=0
DeltaPositivaJ=0
Po=0.2
for i in range(NumeroIteracionesOptimizacion):
    if (DeltaTotalEspera[i]>=0):
        DeltaPositivaEspera=DeltaPositivaEspera+DeltaTotalEspera[i]
    if (DeltaTotalCola[i]>=0):
        DeltaPositivaCola=DeltaPositivaCola+DeltaTotalCola[i]
    if (DeltaJ[i]>=0):
        DeltaPositivaJ=DeltaPositivaJ+DeltaJ[i]
TemperaturaEspera=(DeltaPositivaEspera*(-1.0)/len(DeltaTotalEspera))/log(Po)      
TemperaturaCola=(DeltaPositivaCola*(-1.0)/len(DeltaTotalCola))/log(Po)
TemperaturaJ=(DeltaPositivaJ*(-1.0)/len(DeltaJ))/log(Po) 
print "TemperaturaJ",   TemperaturaJ

TotalEsperaMejorado=[]
TotalColaMejorado=[]
JMejorado=[]

def AsignacionNuevosValores(VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB):
    VerdeNAMejorado=[]
    VerdeEAyOAMejorado=[]
    VerdeNBMejorado=[]
    VerdeEByOBMejorado=[]
    VerdeSBMejorado=[]
    VerdeNAMejorado.append(VerdeNA)
    VerdeEAyOAMejorado.append(VerdeEAyOA)
    VerdeNBMejorado.append(VerdeNB)
    VerdeEByOBMejorado.append(VerdeEByOB)
    VerdeSBMejorado.append(VerdeSB)
    return VerdeNAMejorado, VerdeEAyOAMejorado, VerdeNBMejorado, VerdeEByOBMejorado, VerdeSBMejorado            
    
for i in range(NumeroIteracionesOptimizacion-1):
    print "Solución     ->     ->    ->   ->   ->    ->    ->    ->    ->    ->    ->    ->    ->    ->    ->    ->", i+1

    if (DeltaTotalEspera[i]<0):
        #AsignacionNuevosValores(VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB)
        TotalEsperaMejorado.append(TotalEspera[i])
       
    # Recocido simulado para las colas

    if (DeltaTotalCola[i]<0):
        #AsignacionNuevosValores(VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB)
        TotalColaMejorado.append(TotalCola[i])
     
    # Recocido simulado para la sumatoria de los tiempos de Espera y las colas
    print "J[i]", J[i]  
    if len(JMejorado)<1:
        JMejorado.append(J[i])
    elif (J[i]-JMejorado[len(JMejorado)-1]>0 and TemperaturaJ>0):
        u=uniform(0,1)
        ValorCompararJ=exp(DeltaJ[i]/TemperaturaJ*(-1))
        if (u<ValorCompararJ):      
            VerdeNAMejorado, VerdeEAyOAMejorado, VerdeNBMejorado, VerdeEByOBMejorado, VerdeSBMejorado=AsignacionNuevosValores(VerdeNA, VerdeEAyOA, VerdeNB, VerdeEByOB, VerdeSB)
            JMejorado.append(J[i])
    elif (J[i]-JMejorado[len(JMejorado)-1]<0):
        JMejorado.append(J[i])
    TemperaturaEspera=TemperaturaEspera*alfa
    TemperaturaCola=TemperaturaCola*alfa
    TemperaturaJ=TemperaturaJ*alfa
    print "TemperaturaJ", TemperaturaJ
print "J", J     
pyl.savetxt(r".\JMejorado.txt",[JMejorado],"%02d"," ")
pyl.savetxt(r".\TotalColaMejorado.txt",[TotalColaMejorado],"%02d"," ")     
pyl.savetxt(r".\TotalEsperaMejorado.txt",[TotalEsperaMejorado],"%02d"," ")     

for i in range(0, len(J)-1):
    plt.plot(J, 'r-o')
    plt.plot(JMejorado, 'm-^')
    plt.plot(TotalCola,  'b-*')   
    plt.plot(TotalEspera, 'g-*')     
    pyl.title('Valores J vs. Iteraciones seleccionadas')
xlabel('Valores seleccionados')
ylabel(' J (rojoo) - TotalCola(azul*) - TotalEspera(verde*)')
plt.grid(True,which="both",ls="-")
show()     

for i in range(0, len(JMejorado)-1):
    plt.plot(JMejorado, 'r-o')
    plt.plot(TotalColaMejorado,  'b-*')   
    plt.plot(TotalEsperaMejorado, 'g-*')     
    pyl.title('Valores mejorados vs. Iteraciones seleccionadas')
xlabel('Valores seleccionados')
ylabel(' JMej (rojoo) - TotalColaMej (azul*) - TotalEsperaMej (verde*)')
plt.grid(True,which="both",ls="-")
show()

pyl.savetxt(r".\VerdeNA.txt",[VerdeNA],"%02d"," ")
pyl.savetxt(r".\VerdeEAyOA.txt",[VerdeEAyOA],"%02d"," ")
pyl.savetxt(r".\VerdeNB.txt",[VerdeNB],"%02d"," ")
pyl.savetxt(r".\VerdeEByOB.txt",[VerdeEByOB],"%02d"," ")
pyl.savetxt(r".\VerdeSB.txt",[VerdeSB],"%02d"," ")
plt.plot(VerdeNA, 'r-o') 
plt.plot(VerdeEAyOA, 'm-o')   
plt.plot(VerdeNB,  'g-^')   
plt.plot(VerdeEByOB,  'b-^')   
plt.plot(VerdeSB, 'c-^')
pyl.title('Tiempo de Duracion de cada verde vs. Iteraciones (adim.)')
xlabel('Iteraciones')
ylabel('Tiempo de Duracion de cada verde (seg) ')
plt.grid(True,which="both",ls="-")
show()

print "VerdeNA", VerdeNA
print "VerdeEAyOA", VerdeEAyOA
print "VerdeNB", VerdeNB
print "VerdeEByOB", VerdeEByOB
print "VerdeSB", VerdeSB
del(TTEA)
del(TTEB)
del(TCA)
del(TCB)
del(ColaNorteA)
del(ColaOesteA)
del(ColaEsteA)
del(ColaNorteB)
del(ColaOesteB)
del(ColaEsteB)
del(ColaSurB)
    
del(TiempoEntreSalidasAF1)
del(TiempoEntreSalidasOesteAF2)
del(TiempoEntreSalidasEsteAF2)
del(TiempoEntreSalidasBF1)
del(TiempoEntreSalidasBF2)
del(TiempoEntreSalidasOesteBF3)
del(TiempoEntreSalidasEsteBF3)
del(SalidaNorteAIzq)
del(SalidaNorteADer)
del(SalidaOesteADer)
del(SalidaOesteAIzq)
del(SalidaEsteAIzq)
del(SalidaEsteADer)
del(SalidaNorteBIzq)
del(SalidaNorteBDer)
del(SalidaOesteBDer)
del(SalidaOesteBIzq)
del(SalidaEsteBIzq)
del(SalidaEsteBDer)
del(SalidaSurBIzq)
del(SalidaSurBDer)

del(SalidaDerechaNorteADer)
del(SalidaDerechaOesteAIzq)
del(SalidaDerechaOesteADer)
del(SalidaDerechaEsteAIzq)
del(SalidaDerechaEsteADer)
del(SalidaDerechaNorteBIzq)
del(SalidaDerechaNorteBDer)
del(SalidaDerechaOesteBIzq)
del(SalidaDerechaOesteBDer)
del(SalidaDerechaEsteBIzq)
del(SalidaDerechaEsteBDer)
del(SalidaDerechaSurBIzq)
del(SalidaDerechaSurBDer)
del(SalidaRectoNorteAIzq)
del(SalidaRectoNorteADer)
del(SalidaRectoOesteAIzq)
del(SalidaRectoOesteADer)
del(SalidaRectoEsteAIzq)
del(SalidaRectoEsteADer)
del(SalidaRectoNorteBIzq)
del(SalidaRectoNorteBDer)
del(SalidaRectoOesteBIzq)
del(SalidaRectoOesteBDer)
del(SalidaRectoEsteBIzq)
del(SalidaRectoEsteBDer)
del(SalidaRectoSurBIzq)
del(SalidaRectoSurBDer)
del(SalidaIzquierdaNorteAIzq)
del(SalidaIzquierdaNorteADer)
del(SalidaIzquierdaOesteAIzq)
del(SalidaIzquierdaOesteADer)
del(SalidaIzquierdaEsteAIzq)
del(SalidaIzquierdaEsteADer)
del(SalidaIzquierdaNorteBIzq)
del(SalidaIzquierdaNorteBDer)
del(SalidaIzquierdaOesteBIzq)
del(SalidaIzquierdaOesteBDer)
del(SalidaIzquierdaEsteBIzq)
del(SalidaIzquierdaEsteBDer)
del(SalidaIzquierdaSurBIzq)
del(SalidaIzquierdaSurBDer)

del(PropSalidaIzquierdaNA)
del(PropSalidaRectoNA)
del(PropSalidaDerechaNA)
del(PropSalidaIzquierdaOA)
del(PropSalidaRectoOA)
del(PropSalidaDerechaOA)
del(PropSalidaIzquierdaEA)
del(PropSalidaRectoEA)
del(PropSalidaDerechaEA)
del(PropSalidaIzquierdaNB)
del(PropSalidaRectoNB) 
del(PropSalidaDerechaNB) 
del(PropSalidaIzquierdaOB) 
del(PropSalidaRectoOB) 
del(PropSalidaDerechaOB) 
del(PropSalidaIzquierdaEB) 
del(PropSalidaRectoEB) 
del(PropSalidaDerechaEB) 
del(PropSalidaIzquierdaSB)
del(PropSalidaRectoSB)
del(PropSalidaDerechaSB)

del(Ciclo)
del(Amarillo)
del(TodoRojo)
del(TiempoAcumuladoCiclo)

del(CantVehiculosNorteAIzq)
del(CantVehiculosNorteADer)
del(CantVehiculosOesteAIzq)
del(CantVehiculosOesteADer)
del(CantVehiculosEsteAIzq)
del(CantVehiculosEsteADer)
del(CantVehiculosNorteBIzq)
del(CantVehiculosNorteBDer)
del(CantVehiculosOesteBIzq)
del(CantVehiculosOesteBDer)
del(CantVehiculosEsteBIzq)
del(CantVehiculosEsteBDer)
del(CantVehiculosSurBIzq)
del(CantVehiculosSurBDer)
del(CantCiclos)
del(TiempoDeSimulacion)
del(NorteAIzq)
del(NorteADer) 
del(OesteADer)
del(OesteAIzq)
del(EsteAIzq)
del(EsteADer)
del(NorteBIzq) 
del(NorteBDer) 
del(OesteBDer) 
del(OesteBIzq)
del(EsteBIzq)
del(EsteBDer)
del(SurBIzq)
del(SurBDer) 
del(colaNorteADer)
del(colaNorteAIzq)
del(colaOesteADer)
del(colaOesteAIzq)
del(colaEsteAIzq)
del(colaEsteADer)
del(colaNorteBDer)
del(colaNorteBIzq)
del(colaOesteBDer)
del(colaOesteBIzq)
del(colaEsteBIzq)
del(colaEsteBDer)
del(colaSurBIzq)
del(colaSurBDer)
